Meteor.methods({
  is_user_activated: function () {
    if (Meteor.user() && Meteor.user().emails && Meteor.user().emails[0].verified) {
      return true
    }
    return false
  },

  set_user_locale: function (locale) {
    var user_id = this.user_id
    if (user_id) {
      Meteor.users.update({_id:user_id}, {$set:{
        locale: locale
      }})
    }
    else {
      i18n.setLanguage(locale)
    }
  },
})
