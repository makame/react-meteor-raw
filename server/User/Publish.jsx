Meteor.publishComposite("self", function(){
  return {
    find: function() {
      return Meteor.users.find({_id: this.userId}, {limit: 1})
    },
    children:
    [
      {
        find: function(doc)
        {
          if (doc && doc.avatar != null) {
            return Avatars.find({_id:doc.avatar}, {limit: 1})
          }
          else {
            return undefined
          }
        }
      },
    ]
  }
})
