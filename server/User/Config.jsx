Accounts.config({
  sendVerificationEmail: false,
  forbidClientAccountCreation : false,
  // forbidClientAccountCreation: true,
  // restrictCreationByEmailDomain: "school.edu",
  // loginExpirationDays: 30,
  // oauthSecretKey: "dgfdfdgdgfnbv",
})

// Accounts.validateNewUser(function(user) {
//   var error = {}
//
//   // error.password_2 = i18n('registration.message.error_password_one_wrong', 4)
//
//   if (_.keys(error).length > 0) {
//     throw new Meteor.Error(403, error)
//   }
//   else {
//     return true
//   }
// })

Accounts.onCreateUser(function(options, user) {
  var error = {}

  user.first_name = options.first_name
  user.last_name = options.last_name

  check(user, Meteor.users.simpleSchema())

  if (_.keys(error).length > 0) {
    throw new Meteor.Error(403, error)
  }
  else {
    return user
  }
})
