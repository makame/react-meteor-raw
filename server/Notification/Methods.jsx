Meteor.methods({
  remove_notification: function (data) {
    var userId = Meteor.userId()

    if (!userId) {
      throw new Meteor.Error(500, 'PLEASE LOGIN')
    }

    Future = Npm.require('fibers/future')
    var methFuture = new Future()

    Notification.remove(
      {
        _id: data._id,
        userId: userId
      },
      function (error) {
        if (error) {
          methFuture.throw(error)
        }
        else {
          methFuture.return()
        }
      }
    )

    return methFuture.wait()
  },

  saw_notification: function (data) {
    var userId = Meteor.userId()

    if (!userId) {
      throw new Meteor.Error(500, 'PLEASE LOGIN')
    }

    Future = Npm.require('fibers/future')
    var methFuture = new Future()

    Notification.update(
      {
        _id: data._id,
        userId: userId
      },
      {
        $set:{
          seen: true,
        }
      },
      function (error) {
        if (error) {
          methFuture.throw(error)
        }
        else {
          methFuture.return()
        }
      }
    )

    return methFuture.wait()
  },

  add_notification: function (data) {
    var userId = Meteor.userId()

    if (!userId) {
      throw new Meteor.Error(500, 'PLEASE LOGIN')
    }

    Future = Npm.require('fibers/future')
    var methFuture = new Future()

    Notification.insert(
      {
        userId: userId,
        title: data.title,
        message: data.message,
      },
      function (error) {
        if (error) {
          methFuture.throw(error)
        }
        else {
          methFuture.return()
        }
      }
    )

    return methFuture.wait()
  },
})
