Meteor.publishComposite("notification", function(){
  return {
    find: function() {
      return Notification.find({userId: this.userId}, {limit: 10, sort: {createdAt: -1}})
    },
  }
})
