NotificationsElem = React.createClass({
  render() {
    const self = this

    var notifications = _.map(this.props.notifications, function(item) {
      var onClick = function() {
        Meteor.call("remove_notification", { _id: item._id }, function(error, result){
          if (error) {
            console.log("error", error);
          }
        })
      }

      return (
        <div key={item._id} onClick={onClick} className="notification">
          <div>
            {item.title}
          </div>
          <div className="margin-top-1 text sub mini">
            {item.message}
          </div>
        </div>
      )
    })

    return (
      <ChildsAnim className="notifications" duration={300}>
        {notifications}
      </ChildsAnim>
    )
  }
})
