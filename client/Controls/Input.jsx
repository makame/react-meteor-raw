Input = React.createClass({

  getInitialState() {
    return {
      value: undefined,
      error: false,
      errorMessage: '',
    }
  },

  handleChange: function(e) {
    var value = e.target.value
    this.validate(value)
    if (this.props.json) {
      this.setState({value: value})
      try {
        var valueParsed = JSON.parse(value, JSON.dateParser)
        this.props.linkState.requestChange(valueParsed)
      } catch (e) {
        console.log('error', e);
      }
    }
    else {
      this.setState({value: value})
      this.props.linkState.requestChange(value)
    }
  },

  validate(value) {
    var error = false
    var errorMessage = ''

    if (this.props.check && !(this.props.empty && value == '')) {
      if (this.props.check._schemaKeys) {
        if (this.props.check && !(value == '' && this.props.check._schema && this.props.check._schema[this.props.check._schemaKeys[0]] && this.props.check._schema[this.props.check._schemaKeys[0]].optional)) {
          try {
            var data = {}
            if (this.props.json) {
              data[this.props.check._schemaKeys[0]] = JSON.parse(value, JSON.dateParser)
            }
            else {
              data[this.props.check._schemaKeys[0]] = value
            }
            check(data, this.props.check)
          }
          catch (e) {
            error = true
            // console.log(e)
            if (e.reason) {
              errorMessage = e.reason.replace(/Match error:/gi, '').trim()
            }
            else {
              errorMessage = e.message.replace(/Match error:/gi, '').trim()
            }
          }
        }
      }
      else {
        try {
          check(value, this.props.check)
        }
        catch (e) {
          error = true
          // console.log(e)
          if (e.reason) {
            errorMessage = e.reason.replace(/Match error:/gi, '').trim()
          }
          else {
            errorMessage = e.message.replace(/Match error:/gi, '').trim()
          }
        }
      }
    }

    this.setState({error: error, errorMessage: errorMessage})
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.json) {
      var value = nextProps.linkState.value
      if (value == undefined) {
        value = null
      }
      this.setState({value: this.stringify(value)})
      this.validate(this.stringify(value))
    }
    else {
      this.setState({value: nextProps.linkState.value})
      this.validate(nextProps.linkState.value)
    }
    // this.validate(this.refs.input.value)
  },

  componentDidMount() {
    if (this.props.json) {
      var value = this.props.linkState.value
      if (value == undefined) {
        value = null
      }
      this.setState({value: this.stringify(value)})
      this.validate(this.stringify(value))
    }
    else {
      this.setState({value: this.props.linkState.value})
      this.validate(this.props.linkState.value)
    }
    // this.validate(this.refs.input.value)
  },

  onKeyUp: function(e) {
    if (e.which == 13) {
      if (this.props.onEnter) {
        this.props.onEnter()
      }
    }
  },

  stringify(obj) {
    return JSON.stringify(obj)
  },

  render() {
    const {check, className, classNameInput, json, classNameError, showError, type, ...props} = this.props

    var classNameInputRender = classNameInput

    if (this.props.label) {
      classNameInputRender += ' margin-top-1'
    }

    if (this.state.error) {
      classNameInputRender += ' error'
    }

    var error = null

    if (showError) {
      error = (
        <FadeScaleText className={"margin-top-1 text error " + classNameError}>
          {this.state.errorMessage}
        </FadeScaleText>
      )
    }

    var input = (<input ref="input" className={classNameInputRender} type={type} value={this.state.value} onChange={this.handleChange} onKeyUp={this.onKeyUp} {...props}/>)

    if (type == 'textarea') {
      input = (<textarea ref="input" className={classNameInputRender} type={type} value={this.state.value} onChange={this.handleChange} onKeyUp={this.onKeyUp} {...props}></textarea>)
    }

    return (
      <div className={className}>
        {this.props.label ? (<div className={"container center align-left input-label " + classNameInput}>{this.props.label}</div>) : ''}
        {input}
        {error}
      </div>
    )
  }
});
