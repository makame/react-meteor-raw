PopupMenu = React.createClass({
  render() {
    return (
      <div className="popup-menu">
        {this.props.children}
      </div>
    )
  }
})
