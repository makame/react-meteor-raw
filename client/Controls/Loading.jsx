loadingService = {};

LoadingContainer = React.createClass({

  getInitialState: function() {
    return {
      items: [],
    }
  },

  componentWillMount(){
    var self = this;

    loadingService.on = function(id, simple) {
      if (_.where(self.state.items, {id: id}).length == 0) {
        self.state.items.push({
          id, simple
        })
        self.setState(self.state.items);
      }
    };

    loadingService.off = function(id) {
      while (_.where(self.state.items, {id: id}).length > 0) {
        for (var i = 0; i < self.state.items.length; i++) {
          if (self.state.items[i].id == id) {
            self.state.items.splice(i, 1)
            break
          }
        }
      }
      self.setState(self.state.items);
    };

    loadingService.isBusy = function(id) {
      return _.where(self.state.items, {id: id}).length != 0
    };
  },

  // shouldComponentUpdate: function(nextProps, nextState) {
  //   if (nextState.show != this.state.show) {
  //     return true
  //   }
  //   return false
  // },

  render: function () {
    var self = this
    var props = {};

    var loadingClassName = "loading"

    if (self.state.items.length > 0) {
      loadingClassName += " show"
    }

    var simple = true

    for (var i = 0; i < this.state.items.length; i++) {
      if (!this.state.items[i].simple) {
        simple = false
        break
      }
    }

    if (simple) {
      loadingClassName += " simple"
    }

    // var bigLoading = (
    //   <div className={loadingClassName}>
    //     <div className="loading-container">
    //       <div className="circle-1">
    //         <div className="circle-2">
    //           <div className="circle-3">
    //             <div className="circle-4">
    //               <div className="circle-5">
    //                 <div className="circle-6">
    //                   <div className="circle-7">
    //                     <div className="circle-8">
    //                     </div>
    //                   </div>
    //                 </div>
    //               </div>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </div>
    //     <div className="loading-container-label">
    //       [LOADING]
    //     </div>
    //   </div>
    // )

    var loading = (
      <div className={loadingClassName}>
        <div className="loading-bar"></div>
      </div>
    )

    return loading;
  }
});
