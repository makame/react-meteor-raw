Editor = React.createClass({

  getInitialState() {
    return {
      items: [
        [
          {text: 'Code, makame!', attrs: { code: true }},
        ],
      ]
    }
  },

  getDefaultProps() {
    return {
      linkState: null
    }
  },

  componentDidMount() {
    var self = this
  },

  getSelectedNode() {
    if (document.selection)
    return document.selection.createRange().parentElement();
    else
    {
      var selection = window.getSelection();
      if (selection.rangeCount > 0)
      return selection.getRangeAt(0).startContainer.parentNode;
    }
  },

  replaceSelection(html, selectInserted) {
    var sel, range, fragment;

    if (typeof window.getSelection != "undefined") {
      // IE 9 and other non-IE browsers
      sel = window.getSelection();

      // Test that the Selection object contains at least one Range
      if (sel.getRangeAt && sel.rangeCount) {
        // Get the first Range (only Firefox supports more than one)
        range = window.getSelection().getRangeAt(0);
        range.deleteContents();

        // Create a DocumentFragment to insert and populate it with HTML
        // Need to test for the existence of range.createContextualFragment
        // because it's non-standard and IE 9 does not support it
        if (range.createContextualFragment) {
          fragment = range.createContextualFragment(html);
        } else {
          // In IE 9 we need to use innerHTML of a temporary element
          var div = document.createElement("div"), child;
          div.innerHTML = html;
          fragment = document.createDocumentFragment();
          while ( (child = div.firstChild) ) {
            fragment.appendChild(child);
          }
        }
        var firstInsertedNode = fragment.firstChild;
        var lastInsertedNode = fragment.lastChild;
        range.insertNode(fragment);
        if (selectInserted) {
          if (firstInsertedNode) {
            range.setStartBefore(firstInsertedNode);
            range.setEndAfter(lastInsertedNode);
          }
          sel.removeAllRanges();
          sel.addRange(range);
        }
      }
    } else if (document.selection && document.selection.type != "Control") {
      // IE 8 and below
      range = document.selection.createRange();
      range.pasteHTML(html);
    }
  },

  getSelectionHtml() {
    var html = "";
    if (typeof window.getSelection != "undefined") {
      var sel = window.getSelection();
      if (sel.rangeCount) {
        var container = document.createElement("div");
        for (var i = 0, len = sel.rangeCount; i < len; ++i) {
          container.appendChild(sel.getRangeAt(i).cloneContents());
        }
        html = container.innerHTML;
      }
    }
    else if (typeof document.selection != "undefined") {
      if (document.selection.type == "Text") {
        html = document.selection.createRange().htmlText;
      }
    }
    return html;
  },

  handleChange(e) {
    // console.log(this.refs.input.value());
    var items = this.calcHtml(this.refs.input.value())
    this.state.items = items
    this.setState(this.state.items)

    if (this.props.linkState) {
      this.props.linkState.requestChange(this.state.items)
    }
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.linkState) {
      this.setState({items: nextProps.linkState.value})
    }
  },

  componentDidMount() {
    if (this.props.linkState) {
      this.setState({items: this.props.linkState.value})
    }
  },

  renderRowAlt(self, row) {
    return _.map(row, function(item) {
      return self.renderItem(self, item)
    }).join("")
  },

  renderRow(self, row) {
    return '<div>' + _.map(row, function(item) {
      return self.renderItem(self, item)
    }).join("") + '</div>'
  },

  renderItem(self, item) {

    if (!!item.attrs.src) {
      return '<img src="' + item.attrs.src + '">'
    }

    if (!!item.attrs.href) {
      if (item.text.trim() == '') {
        return ''
      }
      return '<a href="' + item.attrs.href + '">' + item.text + '</a>'
    }

    var classes = [];
    if (item.attrs.bold) {
      classes.push("bold")
    }
    if (item.attrs.underline) {
      classes.push("underline")
    }
    if (item.attrs.overline) {
      classes.push("overline")
    }
    if (item.attrs.header) {
      classes.push("header")
    }
    if (item.attrs.italic) {
      classes.push("italic")
    }
    if (item.attrs.bigheader) {
      classes.push("bigheader")
    }
    if (item.attrs.mini) {
      classes.push("mini")
    }
    if (item.attrs.code) {
      classes.push("code")
    }

    var text = item.text

    if (classes.length == 0) {
      return text
    }

    return '<span class="' + classes.join(' ') + '">' + text + '</span>'
  },

  calcHtml(value, setSelection) {
    var self = this

    var result = [];

    $('<div/>').append($.parseHTML(value)).contents().each(function(index, itemRow) {
      var jitemRow = $(itemRow)
      var tagRow = jitemRow.prop("tagName")

      var bold = jitemRow.hasClass('bold')
      var underline = jitemRow.hasClass('underline')
      var overline = jitemRow.hasClass('overline')
      var header = jitemRow.hasClass('header')
      var italic = jitemRow.hasClass('italic')
      var code = jitemRow.hasClass('code')
      var bigheader = jitemRow.hasClass('bigheader')
      var mini = jitemRow.hasClass('mini')
      var href = jitemRow.attr('href')
      var selection = setSelection

      if (tagRow == 'DIV' || tagRow == 'SPAN' || tagRow == 'A') {
        result.push(self.getContent(self, jitemRow,
          {
            bold, underline, overline, header, italic, code, bigheader, mini, href, selection
          },
          setSelection
        ))
      }
      else if (tagRow == 'IMG') {
        var src = jitemRow.attr('src')
        result.push([
          {
            text: "",
            attrs: {
              src
            }
          }
        ])
      }
      else {
        result.push([
          {
            text: $('<div/>').append(jitemRow.clone()).html(),
            attrs: {
              bold, underline, overline, header, italic, code, bigheader, mini, selection
            }
          }
        ])
      }
    })

    // console.log(result);

    result = this.optimizeData(result)

    return result
  },

  getContent(self, jitemRow, attrs, setSelection) {
    var result = [];

    jitemRow.contents().each(function(index, item) {
      var jitem = $(item)
      var tag = jitem.prop("tagName")

      var bold = jitem.hasClass('bold') ? true : attrs.bold
      var underline = jitem.hasClass('underline') ? true : attrs.underline
      var overline = jitem.hasClass('overline') ? true : attrs.overline
      var header = jitem.hasClass('header') ? true : attrs.header
      var italic = jitem.hasClass('italic') ? true : attrs.italic
      var code = jitem.hasClass('code') ? true : attrs.code
      var bigheader = jitem.hasClass('bigheader') ? true : attrs.bigheader
      var mini = jitem.hasClass('mini') ? true : attrs.mini
      var href = !!attrs.href ? attrs.href : jitem.attr('href')
      var selection = setSelection

      if (tag == 'DIV' || tag == 'SPAN' || tag == 'A') {
        $.merge(result, self.getContent(self, jitem,
          {
            bold, underline, overline, header, italic, code, bigheader, mini, href, selection
          },
          setSelection
        ))
      }
      else if (tag == 'IMG') {
        var src = jitem.attr('src')
        result.push({
          text: "",
          attrs: {
            src
          }
        })
      }
      else {
        result.push({
          text: $('<div/>').append(jitem.clone()).html(),
          attrs: {
            bold, underline, overline, header, italic, code, bigheader, mini, href, selection
          }
        })
      }
    })

    return result
  },

  optimizeData(data) {
    var self = this
    return _.map(data, function(row) {
      var optimRow = []
      var optimItem = null
      for (var i = 0; i < row.length; i++) {
        if (optimItem != null && self.compareItemAttr(row[i], optimItem)) {
          optimItem = {
            text: optimItem.text + row[i].text,
            attrs: optimItem.attrs,
          }
        }
        else if (optimItem == null) {
          optimItem = row[i]
        }
        else {
          optimRow.push(optimItem)
          optimItem = row[i]
        }

        if (row.length - 1 == i) {
          optimRow.push(optimItem)
        }
      }
      return optimRow
    })
  },

  compareItemAttr(left, right) {
    return left.attrs.bold == right.attrs.bold &&
    left.attrs.underline == right.attrs.underline &&
    left.attrs.overline == right.attrs.overline &&
    left.attrs.header == right.attrs.header &&
    left.attrs.italic == right.attrs.italic &&
    left.attrs.code == right.attrs.code &&
    left.attrs.bigheader == right.attrs.bigheader &&
    left.attrs.mini == right.attrs.mini &&
    left.attrs.href == right.attrs.href &&
    left.attrs.src == right.attrs.src
  },

  selectElementContents(el, collapse) {
    var range = document.createRange();
    range.selectNodeContents(el);
    if (collapse) {
      range.collapse(false);
    }
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  },

  selectNextNode(el) {
    var range = document.createRange();
    var sel = window.getSelection();
    range.selectNodeContents(sel.focusNode);
    range.collapse(false);
    sel.removeAllRanges();
    sel.addRange(range);
  },

  getParagraph() {
    var selected = this.getSelectedNode()
    var ref = ReactDOM.findDOMNode(this.refs.input)

    if (selected.parentNode.parentNode == ref) {
      return selected.parentNode
    }

    if (selected.parentNode != ref) {
      return selected
    }
  },

  onKeyDown(e) {
    if ($(this.getSelectedNode()).parent().parent()[0] != ReactDOM.findDOMNode(this.refs.input) && $(this.getSelectedNode()).parent()[0] != ReactDOM.findDOMNode(this.refs.input)) {
      var contents = $(ReactDOM.findDOMNode(this.refs.input)).contents()
      var sel = window.getSelection()
      var pos = sel.anchorOffset - 1
      if (sel.anchorOffset == 0) {
        pos = 0
      }
      if (this.state.items.length == 1 && this.state.items[0].length == 1) {
        console.log(contents);
        this.selectElementContents(contents[pos], true)
      }
    }
  },

  onKeyUp(e) {

    this.selection = this.saveSelection()

    if (e.which == 13) {
      if (this.props.onEnter) {
        this.props.onEnter(this)
      }
    }
    else if (e.which == 8) {
      if (this.props.onBack) {
        this.props.onBack(this)
      }
    }
    else if (e.which == 37) {
      if (this.props.onLeft) {
        this.props.onLeft(this)
      }
    }
    else if (e.which == 39) {
      if (this.props.onRight) {
        this.props.onRight(this)
      }
    }
  },

  onClick() {
    this.selection = this.saveSelection()
  },

  value() {
    return this.state.items
  },

  saveSelection() {
    if (window.getSelection) {
      sel = window.getSelection();
      if (sel.getRangeAt && sel.rangeCount) {
        return sel.getRangeAt(0);
      }
    } else if (document.selection && document.selection.createRange) {
      return document.selection.createRange();
    }
    return null;
  },

  restoreSelection(range) {
    if (range) {
      if (window.getSelection) {
        sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
      } else if (document.selection && range.select) {
        range.select();
      }
    }
  },

  selection: null,

  setBold() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.bold = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items, true)

    this.handleChange()
  },

  desetBold() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.bold = false
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setItalic() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.italic = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetItalic() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.italic = false
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setHeader() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.header = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetHeader() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.header = false
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setCode() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.code = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetCode() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.code = false
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setUnderline() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.underline = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setOverline() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.overline = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetOverline() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.overline = false
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setBigHeader() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.bigheader = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetBigHeader() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.bigheader = false
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setMini() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.mini = true
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetMini() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        if (!item.attrs.href && !item.attrs.src) {
          item.attrs.mini = false
        }
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetAll() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        item.attrs.header = false
        item.attrs.code = false
        item.attrs.bold = false
        item.attrs.italic = false
        item.attrs.overline = false
        item.attrs.underline = false
        item.attrs.bigheader = false
        item.attrs.mini = false
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  setLink(url) {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        item.attrs.header = false
        item.attrs.code = false
        item.attrs.bold = false
        item.attrs.italic = false
        item.attrs.overline = false
        item.attrs.underline = false
        item.attrs.bigheader = false
        item.attrs.mini = false
        item.attrs.href = url
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  desetLink() {
    var self = this

    this.restoreSelection(this.selection)

    var html = this.getSelectionHtml()

    var items = this.calcHtml(html, true)

    _.each(items, function(row) {
      _.each(row, function(item) {
        item.attrs.href = undefined
      })
    })

    items = _.map(items, function(row) {
      return self.renderRowAlt(self, row)
    }).join("")

    this.replaceSelection(items)

    this.handleChange()
  },

  addImage(src) {
    var self = this

    this.restoreSelection(this.selection)

    this.replaceSelection('<img src="' + src + '">')

    this.handleChange()
  },

  render() {
    var self = this
    var html = ""

    if (this.state.items.length == 0 || (this.state.items.length == 1 && this.state.items[0].length == 0) || (this.state.items.length == 1 && this.state.items[0].length == 1 && this.state.items[0][0].text.trim() == '')) {
      html = ""
    }
    else {
      html = _.map(this.state.items, function(row) {
        return self.renderRow(self, row)
      }).join("")
    }

    // console.log(html);

    return (
      <ContentEditable placeholder={this.props.placeholder} className={"editor " + this.props.className} ref="input" html={html} disabled={this.props.disabled} onChange={this.handleChange} onKeyDown={this.onKeyDown} onKeyUp={this.onKeyUp} onMouseUp={this.onClick} onTouchEnd={this.onClick}/>
    );
  }
})

ContentEditable = React.createClass({

  value() {
    return this.htmlEl.innerHTML
  },

  shouldComponentUpdate(nextProps) {
    return !this.htmlEl || nextProps.html !== this.htmlEl.innerHTML ||
    this.props.disabled !== nextProps.disabled;
  },

  componentDidUpdate() {
    if ( this.htmlEl && this.props.html !== this.htmlEl.innerHTML ) {
      this.htmlEl.innerHTML = this.props.html;
    }
  },

  onInput(evt) {
    if (this.props.onInput) {
      this.props.onInput(evt)
    }
    this.emitChange(evt)
  },

  onBlur(evt) {
    if (this.props.onBlur) {
      this.props.onBlur(evt)
    }
    this.emitChange(evt)
  },

  emitChange(evt) {
    if (!this.htmlEl) return;
    var html = this.htmlEl.innerHTML;
    if (this.props.onChange && html !== this.lastHtml) {
      evt.target = { value: html };
      this.props.onChange(evt);
    }
    this.lastHtml = html;
  },

  render() {
    return React.createElement(
      this.props.tagName || 'span',
      Object.assign({}, this.props, {
        ref: (e) => this.htmlEl = e,
        onInput: this.onInput,
        onBlur: this.onBlur,
        onClick: this.onClick,
        contentEditable: !this.props.disabled,
        dangerouslySetInnerHTML: {__html: this.props.html}
      }),
      this.props.children
    );
  }
})
