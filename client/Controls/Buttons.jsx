Buttons = React.createClass({
  render() {
    const {className, children, ...props} = this.props
    return <div className={'buttons ' + className} {...props}>{children}</div>
  }
})

Button = React.createClass({
  onClickButton(e) {
    if (!this.props.loading && this.props.onClick) {
      this.props.onClick(e)
    }
  },

  render() {
    var {className, onClick, loading, children, ...props} = this.props

    if (loading) {
      className += ' load'
    }

    return <div className={className} onClick={this.onClickButton} {...props}>{children}</div>
  }
})
