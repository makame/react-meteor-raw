windowService = {}

PopupElement = React.createClass({

  getInitialState: function() {
    return {guid: guid()}
  },

  getDefaultProps: function() {
    return { on: 'hover', back: false, html: '', position: 'top center', popupClassName: '', offset: 9, delay: 300, tagName: 'div' }
  },

  show: function(e) {
    if (e) {
      e.stopPropagation()
      e.preventDefault()
    }

    var element = $(ReactDOM.findDOMNode(this))

    if (this.props.element) {
      element = $(ReactDOM.findDOMNode(this.props.element))
    }

    windowService.showPopup({html: this.props.html, guid: this.state.guid, className: this.props.popupClassName, offset: this.props.offset}, element, this.props.back, this.props.position)
  },

  hide: function(e) {
    if (e) {
      e.stopPropagation()
      e.preventDefault()
    }

    var delay = this.props.delay

    if (this.props.on == 'click') {
      delay = null
    }

    windowService.hidePopup(this.state.guid, this.props.delay)
  },

  componentWillReceiveProps: function(nextProps) {
    this.props = nextProps
    if (nextProps.on) {
      if (nextProps.show) {
        this.show()
      }
      else {
        this.hide()
      }
    }
    this.setState({on: nextProps.on})
  },

  componentDidMount: function() {
    this.setState({on: this.props.on})
  },

  render: function () {
    if (this.state.on == "manual") {
      return React.createElement(
        this.props.tagName || 'div',
        Object.assign({}, this.props, {
          className: this.props.className
        }),
        this.props.children
      )
    }
    else if (this.state.on == "click") {
      return React.createElement(
        this.props.tagName || 'div',
        Object.assign({}, this.props, {
          onClick: this.show,
          onTouchStart: this.show,
          className: this.props.className
        }),
        this.props.children
      )
    }
    else if (this.state.on == "hover") {
      return React.createElement(
        this.props.tagName || 'div',
        Object.assign({}, this.props, {
          onMouseOver: this.show,
          onMouseOut: this.hide,
          onTouchStart: this.show,
          onTouchEnd: this.hide,
          className: this.props.className
        }),
        this.props.children
      )
    }
    else {
      return null
    }
  }

})

ModalElement = React.createClass({

  guid: guid(),

  getDefaultProps() {
    return {
      tagName: 'div'
    }
  },

  show: function(e) {
    e.stopPropagation()
    e.preventDefault()

    windowService.showModal({html: this.props.html, guid: this.guid, offset: this.props.offset})
  },

  hide: function(e) {
    windowService.hideModal(this.guid)
  },

  render: function () {
    return React.createElement(
      this.props.tagName || 'div',
      Object.assign({}, this.props, {
        onClick: this.show,
        className: this.props.className
      }),
      this.props.children
    )
  }

})

WindowContainer = React.createClass({

  timeoutPopup: null,
  delayPopup: null,

  stopDelayPopup() {
    clearTimeout(this.timeoutPopup)
  },

  restartDelayPopup() {
    var self = this
    if (self.delayPopup) {
      clearTimeout(self.timeoutPopup)
      self.timeoutPopup = setTimeout(function(){
        self.state.popup = null
        if (self.state.element) {
          self.state.element.removeClass('z-index-101')
        }
        self.setState({ popup: self.state.popup, element: null })
      }, self.delayPopup)
    }
  },

  componentWillMount(){
    var self = this

    windowService.showPopup = function(popup, element, back, position) {
      if (!back) {
        back = false
      }

      var offset = 9

      if (popup && popup.offset != null) {
        offset = popup.offset
      }

      if (self.state.element) {
        self.state.element.removeClass('z-index-101')
      }

      self.setState({ popup: popup, element, back, position, offset: offset })
    }

    windowService.hidePopup = function(guid, delay) {
      clearTimeout(self.timeoutPopup)
      if (guid) {
        if (self.state.popup && self.state.popup.guid == guid) {
          self.delayPopup = delay
          if (!!self.delayPopup) {
            self.timeoutPopup = setTimeout(function(){
              self.state.popup = null
              self.setState({ popup: self.state.popup, element: null })
            }, self.delayPopup)
          }
          else {
            self.state.popup = null
            self.setState({ popup: self.state.popup, element: null })
          }
        }
      }
      else {
        self.delayPopup = null
        self.state.popup = null
        self.setState({ popup: self.state.popup, element: null })
      }
    }

    windowService.hideFront = function() {
      self.hideFront()
    }

    windowService.showModal = function(modal) {
      self.restartDelayPopup()
      self.setState({ modal: modal })
    }

    windowService.hideModal = function() {
      self.state.modal = null
      self.setState({ modal: self.state.modal })
    }

    windowService.update = function() {
      self.componentDidUpdate()
    }
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true
  },

  hideModal: function(e) {
    this.state.modal = null
    this.setState({modal: this.state.modal})
  },

  hidePopup: function(e) {
    this.state.popup = null
    if (this.state.element) {
      this.state.element.removeClass('z-index-101')
    }
    this.setState({popup: this.state.popup, element: null})
  },

  hideWindow: function(e) {
    this.state.window = null
    this.setState({window: this.state.window, element: null})
  },

  getInitialState: function() {
    return {modal: null, popup: null, element: null, padding_window: 10, position: 'top center', offset: 9}
  },

  componentDidUpdate: function() {
    if (this.state.modal) {
      var modal = $(ReactDOM.findDOMNode(this.refs.modal))

      var min_x = $(window).scrollLeft()
      var min_y = $(window).scrollTop()

      var max_x = $(window).width() + $(window).scrollLeft()
      var max_y = $(window).height() + $(window).scrollTop()

      var modal_height = modal.outerHeight()
      var modal_width = modal.outerWidth()

      // var top = min_y + (max_y - min_y) / 2 - modal_height / 2
      var top = 30 + min_y
      var left = min_x + (max_x - min_x) / 2 - modal_width / 2

      top = _.max([top, 30])

      modal.offset({top: top, left: left})
    }

    if (this.state.popup) {
      var popup = $(ReactDOM.findDOMNode(this.refs.popup))
      if (popup) {
        var element = this.state.element

        // if (this.state.back) {
        //   var style = element.getStyleObject()
        //   this.element = element.clone().css(style).addClass('z-index-101')
        //   this.element.offset({ top: element.offset().top, left: element.offset().left })
        //   $(ReactDOM.findDOMNode(this.refs.window)).prepend(this.element)
        // }

        var min_x = $(window).scrollLeft()
        var min_y = $(window).scrollTop()

        var max_x = $(window).width() + $(window).scrollLeft()
        var max_y = $(window).height() + $(window).scrollTop()

        var this_y = element.offset().top
        var this_x = element.offset().left

        var this_height = element.outerHeight()
        var this_width = element.outerWidth()

        var this_y_middle = this_y + this_height / 2
        var this_x_middle = this_x + this_width / 2

        var popup_height = popup.outerHeight()
        var popup_width = popup.outerWidth()

        var top = this_y
        var left = this_x

        var want_y = 'auto'
        var want_x = 'auto'

        var result_y = 'auto'
        var result_x = 'auto'
        var result_alter_y = 'center'

        if (this.state.position == 'top center' || this.state.position == 'top') {
          top = this_y - popup_height - this.state.offset
          left = this_x + this_width / 2 - popup_width / 2

          result_y = "top"
          result_x = "center"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window
            result_x = "left"
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window
            result_x = "right"
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y + this_height + this.state.offset
            result_y = "bottom"
          }
        }

        if (this.state.position == 'top left') {
          top = this_y - popup_height - this.state.offset
          left = this_x - popup_width + 22 + this_width / 2

          result_y = "top"
          result_x = "left"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window
            result_x = "left"
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window
            result_x = "right"
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.offset
            result_y = "bottom"
          }
        }

        if (this.state.position == 'top right') {
          top = this_y - popup_height - this.state.offset
          left = this_x + this_width - 22 - this_width / 2

          result_y = "top"
          result_x = "right"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window
            result_x = "left"
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window
            result_x = "right"
          }

          if (top < this_height - this.state.padding_window) {
            top = this_y + this_height + this.state.offset
            result_y = "bottom"
          }
        }

        if (this.state.position == 'bottom center' || this.state.position == 'bottom') {
          top = this_y + this_height + this.state.offset
          left = this_x + this_width / 2 - popup_width / 2

          result_y = "bottom"
          result_x = "center"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window
            result_x = "left"
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window
            result_x = "right"
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset
            result_y = "top"
          }
        }

        if (this.state.position == 'bottom left') {
          top = this_y + this_height + this.state.offset
          left = this_x - popup_width + 22 + this_width / 2

          result_y = "bottom"
          result_x = "left"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window
            result_x = "left"
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window
            result_x = "right"
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset
            result_y = "top"
          }
        }

        if (this.state.position == 'bottom right') {
          top = this_y + this_height + this.state.offset
          left = this_x + this_width - 22 - this_width / 2

          result_y = "bottom"
          result_x = "right"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = max_x - popup_width - this.state.padding_window
            result_x = "left"
          }
          else if (left < this.state.padding_window) {
            left = this.state.padding_window
            result_x = "right"
          }

          if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height - this.state.offset
            result_y = "top"
          }
        }

        if (this.state.position == 'left') {
          top = this_y - popup_height / 2 + this_height / 2
          left = this_x - popup_width - this.state.offset

          result_y = "center"
          result_x = "left"
          result_alter_y = "center"

          if (left < this.state.padding_window) {
            left = this_x + this_width + this.state.offset
            result_x = "right"
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y - this_height + this.state.padding_window
            result_alter_y = "bottom"
          }
          else if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height + this_height - this.state.padding_window
            result_alter_y = "top"
          }
        }

        if (this.state.position == 'right') {
          top = this_y - popup_height / 2 + this_height / 2
          left = this_x + this_width + this.state.offset

          result_y = "center"
          result_x = "right"
          result_alter_y = "center"

          if (left + popup_width > max_x - this.state.padding_window) {
            left = this_x - popup_width - this.state.offset
            result_x = "left"
          }

          if (top < this_height + this.state.padding_window) {
            top = this_y - this_height + this.state.padding_window
            result_alter_y = "bottom"
          }
          else if (top + popup_height > max_y - this.state.padding_window) {
            top = this_y - popup_height + this_height - this.state.padding_window
            result_alter_y = "top"
          }
        }

        if (result_y == 'bottom') {
          var left_popup = this_x_middle - left - 8

          left_popup = _.min([left_popup, popup_width - 20])
          left_popup = _.max([left_popup, 20])

          var barrow = popup.find('.barrow').addClass('bottom')
          barrow.css("left", (left_popup - 1) + "px")
          var farrow = popup.find('.farrow').addClass('bottom')
          farrow.css("left", (left_popup) + "px")
        }
        else if (result_y == 'top') {
          var left_popup = this_x_middle - left - 8

          left_popup = _.min([left_popup, popup_width - 20])
          left_popup = _.max([left_popup, 20])

          var barrow = popup.find('.barrow').addClass('top')
          barrow.css("left", (left_popup - 1) + "px")
          var farrow = popup.find('.farrow').addClass('top')
          farrow.css("left", (left_popup) + "px")
        }
        else if (result_y == 'center') {
          if (result_x == 'left') {
            var top_popup = this_y_middle - top - 8

            top_popup = _.min([top_popup, popup_height - 20])
            top_popup = _.max([top_popup, 20])

            var barrow = popup.find('.barrow').addClass('left')
            barrow.css("top", (top_popup - 1) + "px")
            var farrow = popup.find('.farrow').addClass('left')
            farrow.css("top", (top_popup) + "px")
          }
          else if (result_x == 'right') {
            var top_popup = this_y_middle - top - 8

            top_popup = _.min([top_popup, popup_height - 20])
            top_popup = _.max([top_popup, 20])

            var barrow = popup.find('.barrow').addClass('right')
            barrow.css("top", (top_popup - 1) + "px")
            var farrow = popup.find('.farrow').addClass('right')
            farrow.css("top", (top_popup) + "px")
          }
        }

        popup.offset({top: top, left: left})
      }
    }
  },

  hideFront() {
    if (this.state.modal) {
      this.hideModal()
    }
    else if (this.state.popup) {
      this.hidePopup()
    }
  },

  stopPropagation(e) {
    if (e) {
      e.stopPropagation()
      e.preventDefault()
    }
  },

  render: function () {
    var child = []
    var childModal = []
    var back = null

    if (this.state.modal) {
      childModal.push(<div ref="modal" className="modal" key={this.state.modal.guid}>{this.state.modal.html}</div>)
      back = <div className="fullback" key="back" onClick={this.hideFront}></div>
    }
    else if (this.state.popup) {
      if (this.state.back)
      {
        if (this.state.element)
        {
          this.state.element.addClass('z-index-101')
        }
        back = <div className="fullback" key="back" onClick={this.hideFront}></div>
      }
      var finalClassName = "popup"
      if (this.state.popup.className) {
        finalClassName += " " + this.state.popup.className
      }
      child.push(<div ref="popup" className={finalClassName} key={this.state.popup.guid} onMouseOver={this.stopDelayPopup} onMouseOut={this.restartDelayPopup}>{this.state.popup.html}<div className="barrow" ref="barrow"></div><div className="farrow" ref="farrow"></div></div>)
    }

    return (
      <div>
        <ReactCSSTransitionGroup
          component="div"
          exclusive={false}
          transitionName="fade"
          transitionAppear={false}
          transitionAppearTimeout={300}
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}>
          {back}
        </ReactCSSTransitionGroup>

        <Anim
          component="div"
          willEnter={
            function(el, callback){
              el.hide().velocity({ opacity: 0.0, scale: 0.9 }, { duration: 0 })
              .velocity({ opacity: 1.0, scale: 1 }, { display: "inline-block", duration: 150, easing: "swing", complete: callback })
            }
          }
          willAppear={
            function(el, callback){
              el.hide().velocity({ opacity: 0.0, scale: 0.9 }, { duration: 0 })
              .velocity({ opacity: 1.0, scale: 1 }, { display: "inline-block", duration: 150, easing: "swing", complete: callback })
            }
          }
          willLeave={
            function(el, callback){
              el.velocity({ opacity: 0.0, scale: 0.9 }, { display: "none", duration: 150, easing: "swing", complete: callback })
            }
          }>
          {child}
        </Anim>

        <Anim
          component="div"
          willEnter={
            function(el, callback){
              el.hide().velocity({ opacity: 0.0, scale: 0.95 }, { duration: 0 })
              .velocity({ opacity: 1.0, scale: 1 }, { display: "inline-block", duration: 150, easing: "swing", complete: callback })
            }
          }
          willAppear={
            function(el, callback){
              el.hide().velocity({ opacity: 0.0, scale: 0.95 }, { duration: 0 })
              .velocity({ opacity: 1.0, scale: 1 }, { display: "inline-block", duration: 150, easing: "swing", complete: callback })
            }
          }
          willLeave={
            function(el, callback){
              el.velocity({ opacity: 0.0, scale: 0.95 }, { display: "none", duration: 150, easing: "swing", complete: callback })
            }
          }>
          {childModal}
        </Anim>
      </div>
    )
  }

})
