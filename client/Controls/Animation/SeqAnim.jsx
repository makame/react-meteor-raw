class SeqAnimChild extends React.Component {
  shouldComponentUpdate(nextProps: Object): boolean {
    return !!nextProps.shouldUpdate;
  }

  componentWillAppear(callback) {
    console.log("componentWillAppear");
    this._animateIn(callback);
  }

  componentDidAppear() {
    console.log('componentDidAppear');
  }

  componentWillEnter(callback) {
    console.log("componentWillEnter");
    this._animateIn(callback);
  }

  componentDidEnter() {
    console.log('componentDidEnter');
  }

  componentWillLeave(callback) {
    console.log("componentWillLeave");
    this._animateOut(callback);
  }

  componentDidLeave() {
    console.log('componentDidLeave');
  }

  _animateIn(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).hide()
    setTimeout(function() {
      $(el).fadeIn(self.props.animateDuration, callback)
    }, this.props.animateInDelay);
  }

  _animateOut(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    setTimeout(function() {
      $(el).fadeOut(self.props.animateDuration, callback)
    }, this.props.animateOutDelay);
  }

  render(): ?ReactElement {
    var child = this.props.children;
    if (child === null || child === false) {
      return null;
    }
    return React.Children.only(child);
  }
}

SeqAnim = class SeqAnim extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      previousPathname: null
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextContext.location.pathname !== this.context.location.pathname) {
      this.setState({ previousPathname: this.context.location.pathname })
    }
  }

  componentDidUpdate() {
    if (this.state.previousPathname) {
      this.setState({ previousPathname: null })
    }
  }

  render() {
    const { previousPathname } = this.state

    var childCount = React.Children.count(this.props.children);
    var children = React.Children.map(this.props.children, function(child, idx) {
      var inDelay = this.props.delay * idx
      var outDelay = this.props.delay * (childCount - idx - 1)

      return (
        <SeqAnimChild
          key={previousPathname || this.context.location.pathname}
          shouldUpdate={!previousPathname}
          animateDuration={this.props.delay}
          animateInDelay={inDelay}
          animateOutDelay={outDelay}>
          {child}
        </SeqAnimChild>
      )
    }.bind(this));

    return (
      <React.addons.TransitionGroup
        transitionLeave={true}>
        {children}
      </React.addons.TransitionGroup>
    )
  }
}

SeqAnim.contextTypes = {
  location: React.PropTypes.object
}
