ChildsAnim = class ChildsAnim extends React.Component {
  render() {
    const self = this
    const { duration, type, display, children, ...props } = this.props

    return (
      <Anim
        willEnter={
          function(el, callback){
            el.hide()
            if (type == "fade") {
              el.velocity({ opacity: 1.0 }, { display: display ? display : "block", duration: duration, complete: callback })
            }
            else {
              el
              .velocity({ opacity: 0.5, scale: 1.15 }, { display: display ? display : "block", duration: duration / 2 })
              .velocity({ opacity: 1.0, scale: 1 }, { duration: duration / 2, complete: callback })
            }
          }
        }
        willAppear={
          function(el, callback){
            el.hide()
            if (type == "fade") {
              el.velocity({ opacity: 1.0 }, { display: display ? display : "block", duration: duration, complete: callback })
            }
            else {
              el
              .velocity({ opacity: 0.5, scale: 1.15 }, { display: display ? display : "block", duration: duration / 2 })
              .velocity({ opacity: 1.0, scale: 1 }, { duration: duration / 2, complete: callback })
            }
          }
        }
        willLeave={
          function(el, callback){
            el.velocity({ opacity: 0, height: 0, padding: 0, margin: 0 }, { display: "none", duration: duration, complete: callback })
          }
        }
        {...props}>
        {children}
      </Anim>
    )
  }
}
