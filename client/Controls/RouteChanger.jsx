RouteChangerChild = React.createClass({
  shouldComponentUpdate(nextProps: Object): boolean {
    return !!nextProps.shouldUpdate;
  },

  getDefaultProps() {
    return {
      id: guid()
    }
  },

  componentWillAppear(callback) {
    loadingService.on(this.props.id, true)
    this._animateIn(callback)
  },

  componentDidAppear() {
    loadingService.off(this.props.id)
  },

  componentWillEnter(callback) {
    this._animateInDelay(callback)
  },

  componentDidEnter() {
  },

  componentWillLeave(callback) {
    loadingService.on(this.props.id, true)
    this._animateOut(callback)
  },

  componentDidLeave() {
    loadingService.off(this.props.id)
  },

  _animateInDelay(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).hide()
    $(el).velocity({ opacity: 1.0 }, { display: "block", duration: self.props.animateDuration, delay: self.props.animateDuration, complete: callback })
  },

  _animateIn(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).hide()
    $(el).velocity({ opacity: 1.0 }, { display: "block", duration: self.props.animateDuration, complete: callback })
  },

  _animateOut(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).velocity({ opacity: 0 }, { display: "none", duration: self.props.animateDuration, complete: callback })
  },

  render() {
    var child = this.props.children;
    if (child === null || child === false) {
      return null;
    }

    const {id, key, shouldUpdate, animateDuration, ...props} = this.props

    return React.cloneElement(this.props.children, {
      ...props,
      children: React.Children.only(this.props.children)
    });

    // return React.Children.only(this.props.children)
  }
})

RouteChanger = class RouteChanger extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      previousPathname: null
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextContext.location.pathname !== this.context.location.pathname) {
      this.setState({ previousPathname: this.context.location.pathname })
    }
  }

  componentDidUpdate() {
    if (this.state.previousPathname) {
      this.setState({ previousPathname: null })
    }
  }

  render() {
    const { previousPathname } = this.state
    const { duration, ...props } = this.props

    var childCount = React.Children.count(this.props.children);
    var children = React.Children.map(this.props.children, function(child, idx) {
      return (
        <RouteChangerChild
          key={previousPathname || this.context.location.pathname}
          shouldUpdate={!previousPathname}
          animateDuration={this.props.duration}
          {...props}>
          {child}
        </RouteChangerChild>
      )
    }.bind(this));

    return (
      <React.addons.TransitionGroup
        transitionLeave={true}>
        {children}
      </React.addons.TransitionGroup>
    )
  }
}

RouteChanger.contextTypes = {
  location: React.PropTypes.object
}
