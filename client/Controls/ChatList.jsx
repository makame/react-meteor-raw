ChatList = React.createClass({
  scrolled: false,
  timer: null,

  scrollToBottom() {
    ReactDOM.findDOMNode(this.refs.chat).scrollTop = ReactDOM.findDOMNode(this.refs.chat).scrollHeight
    self.scrolled = false
  },

  updateScroll() {
    if (!this.scrolled) {
      this.scrollToBottom()
    }
  },

  prepend(item) {
    var firstMsg = $(ReactDOM.findDOMNode(self.refs.cell)).find('*:first')
    var offset = firstMsg.offset().top - self.refs.chat.scrollTop

    $(ReactDOM.findDOMNode(self.refs.cell)).prepend(item)

    $(ReactDOM.findDOMNode(self.refs.chat)).scrollTop(firstMsg.offset().top - offset)
  },

  append(item) {
    $(ReactDOM.findDOMNode(self.refs.cell)).append(item);
  },

  componentWillUnmount() {
    clearTimeout(this.timer)
  },

  componentDidMount() {
    var self = this

    this.timer = setTimeout(this.updateScroll, 300)

    $(self.refs.chat).on('scroll', function() {
      if (ReactDOM.findDOMNode(self.refs.chat).scrollTop + $(ReactDOM.findDOMNode(self.refs.chat)).height() >= ReactDOM.findDOMNode(self.refs.chat).scrollHeight - 1)
      {
        self.scrolled = false
      }
      else
      {
        self.scrolled = true
      }
    });
  },

  before() {
    if (this.refs.cell) {
      var lastMsg = $(ReactDOM.findDOMNode(this.refs.cell)).find('*:last')
      var firstMsg = $(ReactDOM.findDOMNode(this.refs.cell)).find('*:first')
      var offset = firstMsg.offset().top - ReactDOM.findDOMNode(this.refs.chat).scrollTop
      return {offset, firstMsg, lastMsg}
    }
  },

  after(res) {
    if (!!res) {
      $(ReactDOM.findDOMNode(this.refs.chat)).scrollTop(res.firstMsg.offset().top - res.offset)
      this.updateScroll()

      if (res.lastMsg[0] !== $(ReactDOM.findDOMNode(this.refs.cell)).find('*:last')[0] && !this.scrolled) {
        this.scrollToBottom()
      }
    }
  },

  procedChild(children) {
    var self = this
    return <ChatItem animateDuration={500} will={function(){return self.before()}} did={function(offset){self.after(offset)}} key={children.key}>{children}</ChatItem>
  },

  render() {
    return (
      <div className={"chat " + this.props.className} ref="chat">
        <div className="wrap">
          <React.addons.TransitionGroup
            className="cell" ref="cell"
            transitionLeave={true}>
            {React.Children.map(this.props.children, this.procedChild)}
          </React.addons.TransitionGroup>
        </div>
      </div>
    );
  }
})

ChatItem = React.createClass({
  res: null,

  componentWillMount() {
    this.res = this.props.will()
  },

  componentDidMount() {
    this.props.did(this.res)
  },

  componentWillAppear(callback) {
    this._animateIn(callback)
  },

  componentDidAppear() {
  },

  componentWillEnter(callback) {
    this._animateIn(callback)
  },

  componentDidEnter() {
  },

  componentWillLeave(callback) {
    this._animateOut(callback)
  },

  componentDidLeave() {
  },

  _animateIn(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).css('opacity', 0)
    $(el)
    .velocity({ opacity: 0.5, scale: 1.15 }, { display: "block", duration: self.props.animateDuration / 2 })
    .velocity({ opacity: 1.0, scale: 1 }, { duration: self.props.animateDuration / 2, complete: callback })
  },

  _animateOut(callback) {
    var self = this
    var el = ReactDOM.findDOMNode(this);
    $(el).velocity({ opacity: 0, height: 0, padding: 0, margin: 0 }, { display: "none", duration: self.props.animateDuration, complete: callback })
  },

  render() {
    return (
      <div ref="chatItem" key={this.key}>
        {this.props.children}
      </div>
    );
  }
})
