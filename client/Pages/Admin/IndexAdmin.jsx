IndexAdminPage = React.createClass({
  mixins: [LinkValue],

  getInitialState() {
    return {
    }
  },

  render() {
    return (
      <div className="container p90 center align-center">
        <h2>Admin page</h2>
        <div className="margin-top-2">
          Please select tables or go to <Link to={'/'} activeClassName="active">Main Page</Link>
        </div>
      </div>
    );
  }
});
