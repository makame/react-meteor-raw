RowModal = React.createClass({
  mixins: [LinkValue, ReactMeteorData, GetSubscribe, SchemaMixin],

  getInitialState() {
    return {
      item: undefined,
      callbackError: '',
    }
  },

  getMeteorData() {
    var self = this

    loadingService.on('admin_table_id', false)

    var param = {
      name: self.props.tableName,
      _id: self.props.itemId,
    }

    return this.getSubscribe('admin_table_id', [{name: "admin_table_id", param: param}], function(data) {
      data.item = Admin.getCollection(self.props.tableName).findOne({_id: self.props.itemId})
      return data
    })
  },

  stringify(obj) {
    return JSON.stringify(obj)
  },

  JsonCheck(key) {
    var self = this
    return Match.Where(function(x) {
      check(x, String)
      try {
        var parsed = JSON.parse(x)
        return true
      } catch (e) {
        if (x != '' && Admin.getCollection(self.props.tableName).simpleSchema()._schema[key].type == String) {
          return true
        }
        else {
          throw new Meteor.Error(403, "Match failed")
        }
      }
    })
  },

  syncData() {
    this.state.item = this.data.item
    this.setState({item: this.state.item})
    loadingService.off('admin_table_id')
  },

  saveData() {
    const self = this

    var data = {
      name: self.props.tableName,
      _id: self.props.itemId,
      item: self.state.item,
    }

    loadingService.on('admin_update', true)

    Meteor.call("admin_update", data, function(error, result){
      loadingService.off('admin_update')

      if (error) {
        console.log("error", error);
        self.setState({callbackError: error.reason})
      }
      else {
        self.setState({callbackError: ''})
      }
    });
  },

  removeData() {
    const self = this

    var data = {
      name: self.props.tableName,
      _ids: [self.props.itemId],
    }

    loadingService.on('admin_remove', true)

    Meteor.call("admin_remove", data, function(error, result){
      loadingService.off('admin_remove')

      if (error) {
        console.log("error", error);
        self.setState({callbackError: error.reason})
      }
      else {
        self.setState({callbackError: ''})
      }
    });
  },

  componentDidUpdate() {
    if (this.state.item == undefined) {
      this.syncData()
    }
  },

  render() {
    const self = this

    // windowService.update()

    var schema = Admin.getCollection(this.props.tableName).simpleSchema()
    var keys = schema._firstLevelSchemaKeys

    var cells = _.map(keys, function(key){

      var label = key
      if (schema._schema[key].label) {
        label = schema._schema[key].label
      }

      var valueLink = {
        value: self.state.item ? self.state.item[key] : undefined,
        requestChange: function(newValue) {
          self.state.item[key] = newValue
          self.setState({item: self.state.item})
        }
      }

      if (schema._schema[key].type == Object) {
        return (
          <div key={key} className="margin-top-3">
            <Input classNameInput="widefull" type="textarea" placeholder={label} linkState={valueLink} label={label} check={self.pickSchema(schema, key)} showError={true} json={true} rows="4"></Input>
          </div>
        )
      }
      else if (schema._schema[key].type == Array) {
        return (
          <div key={key} className="margin-top-3">
            <Input classNameInput="widefull" type="textarea" placeholder={label} linkState={valueLink} label={label} check={self.pickSchema(schema, key)}  showError={true} json={true} rows="4"></Input>
          </div>
        )
      }
      else if (schema._schema[key].type == String || schema._schema[key].type == Date) {
        return (
          <div key={key} className="margin-top-3">
            <Input classNameInput="widefull" type="text" placeholder={label} linkState={valueLink} label={label} check={schema.pick(key)} showError={true} json={true} rows="4"></Input>
          </div>
        )
      }
      else {
        return (
          <div key={key} className="margin-top-3">
            <div>{label}</div>
            <div className="margin-top-1">{self.state.item ? self.state.item[key] : undefined}</div>
          </div>
        )
      }
    })

    return (
      <div className="container align-center padding-vertical-4 padding-horizontal-4">
        <div>
          <Buttons>
            <Button onClick={this.syncData}>Reset</Button>
            <Button className="red" onClick={this.removeData} loading={loadingService.isBusy('admin_remove')}>Remove</Button>
            <Button className="blue" onClick={this.saveData} loading={loadingService.isBusy('admin_update')}>Save</Button>
          </Buttons>
          <FadeScaleText className="margin-top-1 text error">
            {this.state.callbackError}
          </FadeScaleText>
          <div>
            {cells}
          </div>
        </div>
      </div>
    )
  }
})
