TablePage = React.createClass({
  mixins: [LinkValue, ReactMeteorData, GetSubscribe],

  getInitialState() {
    return {
      skip: 0,
      limit: 10,
      find: {},
      filters: {},
      sort: {_id: -1},
    }
  },

  getMeteorData() {
    var self = this

    loadingService.on('admin_table', true)

    var param = {
      name: self.props.tableName,
      find: {},
      limit: self.state.limit,
      sort: self.state.sort,
    }

    _.each(self.state.filters, function(item){
      if (item.value != "") {
        try {
          var parsed = JSON.parse(item.value)
          param.find[item.name] = parsed
        } catch (e) {
          if (Admin.getCollection(self.props.tableName).simpleSchema()._schema[item.name].type == String) {
            param.find[item.name] = {"$regex" : item.value}
          }
        }
      }
    })

    return this.getSubscribe('admin_table', [{name: "admin_table", param: param}], function(data) {
      data.table = Admin.getCollection(self.props.tableName).find(param.find, {limit: self.state.limit, sort: self.state.sort}).fetch()

      loadingService.off('admin_table')

      return data
    })
  },

  loadMore() {
    this.state.limit += 10
    this.setState({limit: this.state.limit})
  },

  setSort(value) {
    var desc = -1
    if (this.state.sort[value] == -1) {
      desc = 1
    }
    this.state.sort = {}
    this.state.sort[value] = desc

    this.setState({sort: this.state.sort})
  },

  stringify(obj) {
    return JSON.stringify(obj)
  },

  JsonCheck(key) {
    var self = this
    return Match.Where(function(x) {
      check(x, String)
      try {
        var parsed = JSON.parse(x)
        return true
      } catch (e) {
        if (x != '' && Admin.getCollection(self.props.tableName).simpleSchema()._schema[key].type == String) {
          return true
        }
        else {
          throw new Meteor.Error(403, "Match failed")
        }
      }
    })
  },

  render() {
    const self = this

    var schema = Admin.getCollection(this.props.tableName).simpleSchema()
    var keys = schema._firstLevelSchemaKeys

    var headers = _.map(keys, function(key){
      var setSort = function() {
        self.setSort(key)
      }

      var desc = (<span className="fa fa-sort-amount-desc"></span>)

      if (self.state.sort[key] == -1) {
        desc = (<span className="fa fa-sort-amount-asc"></span>)
      }

      if (schema._schema[key].label) {
        return (<td key={key} className="pointer" onClick={setSort}>{schema._schema[key].label}{desc}</td>)
      }
      else {
        return (<td key={key} className="pointer" onClick={setSort}>{key}{desc}</td>)
      }
    })

    var filters = _.map(keys, function(key) {
      var value = self.state.filters[key] ? self.state.filters[key].value : ''
      var filter = {
        name: key,
        value: value
      }
      self.state.filters[key] = filter

      var valueLink = {
        value: self.state.filters[key].value,
        requestChange: function(newValue) {
          self.state.filters[key].value = newValue
          self.setState({filters: self.state.filters})
        }
      }

      if (schema._schema[key].type == String) {
        return (<td key={key}><Input type="text" placeholder='"Query"' linkState={valueLink} check={self.JsonCheck(key)} empty={true}></Input></td>)
      }
      else {
        return (<td key={key}><Input type="text" placeholder='"Query"' linkState={valueLink} check={self.JsonCheck(key)} empty={true}></Input></td>)
      }
    })

    var rows = _.map(this.data.table, function(item) {
      var cells = _.map(keys, function(key){
        if (typeof item[key] == 'object') {
          return (<td key={key}>{self.stringify(item[key])}</td>)
        }
        else if (typeof item[key] == 'string') {
          return (<td key={key}>{item[key]}</td>)
        }
        else {
          return (<td key={key}>{item[key]}</td>)
        }
      })

      var modal = (<RowModal tableName={self.props.tableName} itemId={item._id}/>)

      return(
        <ModalElement key={item._id} html={modal} tagName="tr" className="pointer">
          <td onClick={function(e){e.stopPropagation()}}>
            <input type="checkbox"></input>

          </td>
          {cells}
        </ModalElement>
      )
    })

    return (
      <div className="container p90 center align-center">
        <h2>Table {Admin.getTitle(self.props.tableName)}</h2>

        <div className="scrollable horizontal">
          <table className="table font-small">
            <thead>
              <tr>
                <td></td>
                {headers}
              </tr>
              <tr>
                <td></td>
                {filters}
              </tr>
            </thead>
            <ChildsAnim component="tbody" duration={300} display="table-row" type="fade">
              {rows}
            </ChildsAnim>
          </table>
        </div>

        <Buttons className="margin-top-2">
          <Button onClick={this.loadMore} loading={loadingService.isBusy('admin_table')}>Load More ({this.state.limit})</Button>
        </Buttons>
      </div>
    )
  }
})
