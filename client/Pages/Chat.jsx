ChatPage = React.createClass({
  getInitialState() {
    return {
      items: [
        (<span key={0}>Chat Message 0</span>),
        (<span key={1}>Chat Message 1</span>),
        (<span key={2}>Chat Message 2</span>),
        (<span key={3}>Chat Message 3</span>),
        (<span key={4}>Chat Message 4</span>),
        (<span key={5}>Chat Message 5</span>),
        (<span key={6}>Chat Message 6</span>),
        (<span key={7}>Chat Message 7</span>),
        (<span key={8}>Chat Message 8</span>),
        (<span key={9}>Chat Message 9</span>),
        (<span key={10}>Chat Message 10</span>),
        (<span key={11}>Chat Message 11</span>)
      ],
    }
  },

  maxKey: 11,

  addItemBefore() {
    this.maxKey++
    this.state.items.unshift(<span key={this.maxKey}>Chat Message {this.maxKey}</span>)
    this.setState(this.state.items)
  },

  addItemAfter() {
    var text = this.refs.text.value
    this.refs.text.value = ""

    this.refs.chat.scrolled = false

    this.maxKey++
    this.state.items.push(<span key={this.maxKey}>{text}</span>)
    this.setState(this.state.items)
  },

  addItemNewSilent() {
    var text = this.refs.text.value
    this.refs.text.value = ""

    this.maxKey++
    this.state.items.push(<span key={this.maxKey}>{text}</span>)
    this.setState(this.state.items)
  },

  render() {
    return (
      <div className="container p90 center align-center">
        <h2>Chat From Bottom to Top List Test</h2>

        <ChatList className="container p90 center" ref="chat">
          {this.state.items}
        </ChatList>

        <div className="margin-top-1">
          <input type="text" ref="text"></input>
        </div>

        <div className="buttons margin-top-1">
          <div onClick={this.addItemBefore}>Add Before</div>
          <div onClick={this.addItemNewSilent}>Add Message Silent</div>
          <div onClick={this.addItemAfter}>Add Message</div>
        </div>
      </div>
    );
  }
});
