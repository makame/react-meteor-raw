EditorPage = React.createClass({
  mixins: [LinkValue],

  getInitialState() {
    return {
      itemsEditor: [
        [
          {text: 'Test, Please', attrs: { header: true, italic: true }},
        ],
        [
          {text: 'Pre text ', attrs: { italic: true }},
          {text: 'Link to VK', attrs: { href: "http://vk.com/" }},
          {text: ' after ', attrs: {}},
          {text: 'it', attrs: { bold: true }},
        ],
        [
          {text: 'Hello, Andromeda!', attrs: { src: 'https://i.ytimg.com/vi/0-4XOCGHfLo/maxresdefault.jpg' }},
        ],
        [
          {text: 'Code, makame!', attrs: { code: true }},
        ],
      ]
    }
  },

  setBold() {
    this.refs.editor.setBold()
  },

  stringify(obj) {
    return JSON.stringify(obj);
  },

  render() {
    return (
      <div className="container p90 center align-center">
        <h2>Editor Test</h2>

        <Editor placeholder="Enter Post" ref="editor" linkState={this.linkValue('itemsEditor')} disabled={false}></Editor>

        <div className="buttons margin-top-1">
          <div onClick={this.setBold}>Set Bold</div>
        </div>

        <h2 className="margin-top-2">Result</h2>

        <div className="margin-top-1 margin-bottom-2" dangerouslySetInnerHTML={{__html: this.stringify(this.state.itemsEditor)}}>
        </div>
      </div>
    );
  }
});
