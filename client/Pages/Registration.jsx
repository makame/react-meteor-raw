RegistrationPage = React.createClass({
  mixins: [LinkValue],

  getInitialState() {
    return {
      username: '',
      email: '',
      password: '',
      password_2: '',
      first_name: '',
      last_name: '',

      callbackLoading: false,
      callbackError: '',
    }
  },

  doRegistration(e) {
    var self = this

    if (self.state.callbackLoading) {
      return
    }

    self.setState({callbackLoading: true})

    var data = {
      first_name: self.state.first_name,
      last_name: self.state.last_name,
      username: self.state.username,
      email: self.state.email,
      password: self.state.password,
    }

    loadingService.on('registration', true)

    Accounts.createUser(data, function(error) {
      self.setState({callbackLoading: false})
      loadingService.off('registration')

      console.log("error", error)
      self.setState({callbackError: error.reason})
      $(e.target).parent().velocity('callout.shake', 500)
    })
  },

  passwordSecondCheck() {
    var self = this
    return Match.Where(function(x) {
      check(x, String)
      if (self.state.password == x)
      {
        return true
      }
      else {
        throw new Meteor.Error(403, "Passwords do not equal")
      }
    })
  },

  componentDidUpdate() {
    if (!!this.props.currentUser) {
      History.pushState(null, "/")
    }
  },

  render() {
    return (
      <div className="container p90 center align-center">
        <h2 className="margin-bottom-1">Registration</h2>

        <div className="inlines">
          <div className="padding-horizontal-2 margin-top-2">
            <Input classNameInput="wide" label="Login:" type="text" ref="text" placeholder="Login" linkState={this.linkValue('username')} check={Meteor.users.simpleSchema().pick('username')} showError={true}/>
          </div>

          <div className="padding-horizontal-2 margin-top-2">
            <Input classNameInput="wide" label="Email:" type="text" ref="text" placeholder="Email" linkState={this.linkValue('email')} check={Meteor.users.simpleSchema().pick('emails.$.address')} showError={true}/>
          </div>
        </div>

        <div className="inlines">
          <div className="padding-horizontal-2 margin-top-2">
            <Input classNameInput="wide" label="Password:" type="password" ref="password" placeholder="Password" linkState={this.linkValue('password')} check={PasswordReqExp} showError={true}/>
          </div>

          <div className="padding-horizontal-2 margin-top-2">
            <Input classNameInput="wide" label="Repeat Password:" type="password" ref="password" placeholder="Repeat Password" linkState={this.linkValue('password_2')} check={this.passwordSecondCheck()} showError={true}/>
          </div>
        </div>

        <div className="inlines">
          <div className="padding-horizontal-2 margin-top-2">
            <Input classNameInput="wide" label="First Name:" type="text" ref="text" placeholder="First Name" linkState={this.linkValue('first_name')} check={Meteor.users.simpleSchema().pick('first_name')} showError={true}/>
          </div>

          <div className="padding-horizontal-2 margin-top-2">
            <Input classNameInput="wide" label="Last Name:" type="text" ref="text" placeholder="Last Name" linkState={this.linkValue('last_name')} check={Meteor.users.simpleSchema().pick('last_name')} showError={true}/>
          </div>
        </div>

        <FadeScaleText className="margin-top-2 text error">
          {this.state.callbackError}
        </FadeScaleText>

        <Buttons className="big margin-top-2">
          <Button className="red" loading={this.state.callbackLoading} onClick={this.doRegistration}>Registration</Button>
        </Buttons>
      </div>
    )
  }
})
