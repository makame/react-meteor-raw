UserPanel = React.createClass({
  mixins: [LinkValue],

  getInitialState() {
    return {
      login: '',
      password: '',

      callbackLoading: false,
      callbackError: '',
    }
  },

  doLogout: function() {
    var self = this

    if (self.state.callbackLoading) {
      return
    }

    self.setState({callbackLoading: true})
    loadingService.on('user_logout', true)

    Meteor.logout(function(err) {
      self.setState({callbackLoading: false})
      self.setState({callbackError: err})
      loadingService.off('user_logout')
      // popupService.hidePopup()
    })
  },

  render() {
    var modal = (<ConfidentialModal/>)

    return (
      <div className="container align-center padding-horizontal-2">
        <div className="margin-top-1 align-left">
          <h3 className="container center">Hello, {this.props.currentUser.username}</h3>
          <ModalElement html={modal} tagName="a" className="absolute top right margin-right-2">
            Privacy Policy
          </ModalElement>
        </div>

        <div className="margin-top-1 align-left">
          Regards, {this.props.currentUser.first_name} {this.props.currentUser.last_name}
        </div>

        <FadeScaleText className="margin-top-2 text error">
          {this.state.callbackError}
        </FadeScaleText>

        <Buttons className="margin-top-2">
          <Button className={"blue"} loading={this.state.callbackLoading} onClick={this.doLogout}>Logout</Button>
        </Buttons>
      </div>
    )
  }
})
