LoginPanel = React.createClass({
  mixins: [LinkValue],

  getInitialState() {
    return {
      login: '',
      password: '',

      callbackLoading: false,
      callbackError: '',
    }
  },

  doAuth: function(e) {
    var self = this

    if (self.state.callbackLoading) {
      return
    }

    self.setState({callbackLoading: true})
    loadingService.on('user_auth', true)

    Meteor.loginWithPassword(this.state.login, this.state.password)

    Accounts.onLoginFailure(function(){
      self.setState({callbackLoading: false})
      self.setState({callbackError: 'Current pair does not exist'})
      loadingService.off('user_auth')
      $(e.target).parent().velocity('callout.shake', 500)
    })

    Accounts.onLogin(function(){
      self.setState({callbackLoading: false})
      self.setState({callbackError: ''})
      loadingService.off('user_auth')
      // popupService.hidePopup()
    })
  },

  render() {
    var modal = (<ConfidentialModal/>)

    return (
      <div className="container align-center padding-top-2">
        <div className="margin-top-1 align-left">
          <div className="container center input-label">Login:</div>
          <ModalElement html={modal} tagName="a" className="absolute top right margin-right-2">
            Privacy Policy
          </ModalElement>
        </div>

        <div className="margin-top-1">
          <Input type="text" ref="text" placeholder="Login" linkState={this.linkValue('login')} check={Meteor.users.simpleSchema().pick('username')} showError={false} empty={true}/>
        </div>

        <div className="margin-top-2 align-left">
          <div className="container center input-label">Password:</div>
        </div>

        <div className="margin-top-1">
          <Input type="password" ref="password" placeholder="Password" linkState={this.linkValue('password')} check={PasswordReqExp} showError={false} empty={true}/>
        </div>

        <FadeScaleText className="margin-top-2 text error">
          {this.state.callbackError}
        </FadeScaleText>

        <Buttons className="margin-top-2">
          <Link className="item" to={'/registration'} activeClassName="active">Registration</Link>
          <Button className={"blue"} loading={this.state.callbackLoading} onClick={this.doAuth}>Login</Button>
        </Buttons>
      </div>
    )
  }
})
