ConfidentialModal = React.createClass({
  render() {
    return (
      <div className="container litle padding-vertical-4 padding-horizontal-4">
        <div>
          <p>
            All information will stay confidential and will not be transfered to the third parties. We do not store any of you application data after the moderation process. We do not store any logs IP's or any of your provider information.
          </p>
          <div>
            <a>Privacy Policy</a>
          </div>
        </div>
      </div>
    )
  }
})
