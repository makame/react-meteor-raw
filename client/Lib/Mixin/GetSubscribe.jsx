GetSubscribe = {
  getSubscribe(id, subs, callback, simple) {
    data = {}

    data.subsReady = false
    loadingService.on(id, simple)

    const subHandles = _.map(subs, function(value) {
      if (value.param) {
        return Meteor.subscribe(value.name, value.param)
      }
      else {
        return Meteor.subscribe(value.name)
      }
    })

    const subsReady = _.all(subHandles, function(handle) {
      return handle.ready()
    })

    data.subsReady = subsReady

    if (data.subsReady) {
      loadingService.off(id)
    }

    if (callback) {
      data = callback(data)
    }

    return data
  },
}
