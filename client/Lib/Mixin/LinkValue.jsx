LinkValue = {
  linkValue(value) {
    var self = this
    return {
      value: this.state[value],
      requestChange: function(newValue) {
        self.state[value] = newValue;
        self.setState(self.state);
      }
    }
  },
}
