SchemaMixin = {
  pickSchema(schema, key) {
    var allKeys = [key];
    _.each(schema.schema(), function(v,k){
      if(k.indexOf(key + '.') == 0){
        allKeys.push(k)
      }
    })
    return schema.pick(allKeys)
  }
}
