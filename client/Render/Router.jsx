const {Router, Route, IndexRoute} = ReactRouter

History = ReactRouter.history.useQueries(ReactRouter.history.createHistory)()

NotFoundPage = React.createClass({
  render() {
    return (
      <div className="container align-center">
        <h1 className="margin-bottom-d3">[404]</h1>
        <h2 className="margin-bottom-1">Page Not Found</h2>
        <div>You can try another route...</div>
      </div>
    )
  }
})

function checkState(nextState, replaceState, callback, reqUser, roles) {
  var onLoginCallback = null
  var onLoginFailureCallback = null
  var checkAndRedirect = function() {
    if (onLoginCallback) {
      onLoginCallback.stop()
    }

    if (onLoginFailureCallback) {
      onLoginFailureCallback.stop()
    }

    if (reqUser === true) {
      if (!Meteor.user()) {
        replaceState(null, '/')
      }

      if (roles) {
        Meteor.subscribe('self', function() {
          if (!Meteor.users.findOne({_id: Meteor.user()._id, roles: {$all: roles}})) {
            replaceState(null, '/')
          }
          $('#loading').removeClass('show')
          callback()
        });
      }
      else {
        $('#loading').removeClass('show')
        callback()
      }
    }
    else if (reqUser === false) {
      if (Meteor.user()) {
        replaceState(null, '/')
      }
      $('#loading').removeClass('show')
      callback()
    }
    else {
      $('#loading').removeClass('show')
      callback()
    }
  }

  if (Meteor.loggingIn()) {
    $('#loading').addClass('show')
    onLoginCallback = Accounts.onLogin(checkAndRedirect)
    onLoginFailureCallback = Accounts.onLoginFailure(checkAndRedirect)
  }
  else {
    checkAndRedirect()
  }
}

function initAuth(nextState, replaceState, callback) {
  checkState(nextState, replaceState, callback)
}

function requireAuth(nextState, replaceState, callback) {
  checkState(nextState, replaceState, callback, true)
}

function requireNotAuth(nextState, replaceState, callback) {
  checkState(nextState, replaceState, callback, false)
}

function requireAdmin(nextState, replaceState, callback) {
  checkState(nextState, replaceState, callback, true, ["admin"])
}

const routes = (
  <Router history={History}>
    <Route component={App}>
      <Route path="/admin" component={AdminLayout} onEnter={requireAdmin}>
        <IndexRoute component={IndexAdminPage}/>
        <Route path="table/:tableName" component={TablePage}/>
        <Route path="*" component={NotFoundPage}/>
      </Route>
      <Route path="/" component={HomeLayout} onEnter={initAuth}>
        <Route path="registration" component={RegistrationPage} onEnter={requireNotAuth}/>
        <Route path="editor" component={EditorPage} onEnter={requireAuth}/>
        <Route path="chat" component={ChatPage}/>
        <IndexRoute component={EditorPage}/>
        <Route path="*" component={NotFoundPage}/>
      </Route>
    </Route>
  </Router>
)

Meteor.startup(function() {
  ReactDOM.render(routes, document.getElementById("app-container"))
})

Link = React.createClass({
  onClick(e) {
    windowService.hideModal()
    windowService.hidePopup()

    if (this.props.onClick) {
      this.props.onClick(e)
    }
  },

  render() {
    const {onClick, children, ...props} = this.props
    return <ReactRouter.Link {...props} onClick={this.onClick}>{children}</ReactRouter.Link>
  }
})

ReactCSSTransitionGroup = React.addons.CSSTransitionGroup
LinkedStateMixin = React.addons.LinkedStateMixin

// class StaticContainer extends React.Component {
//   shouldComponentUpdate(nextProps: Object): boolean {
//     return !!nextProps.shouldUpdate
//   }
//
//   render(): ?ReactElement {
//     var child = this.props.children
//     if (child === null || child === false) {
//       return null
//     }
//     return React.Children.only(child)
//   }
// }
//
// RouteCSSTransitionGroup = class RouteCSSTransitionGroup extends React.Component {
//   constructor(props, context) {
//     super(props, context)
//
//     this.state = {
//       previousPathname: null
//     }
//   }
//
//   componentWillReceiveProps(nextProps, nextContext) {
//     if (nextContext.location.pathname !== this.context.location.pathname) {
//       this.setState({ previousPathname: this.context.location.pathname })
//     }
//   }
//
//   render() {
//     const { children, ...props } = this.props
//     const { previousPathname } = this.state
//     return (
//       <ReactCSSTransitionGroup {...props}>
//         <StaticContainer
//           key={previousPathname || this.context.location.pathname}
//           shouldUpdate={!previousPathname}>
//           {children}
//         </StaticContainer>
//       </ReactCSSTransitionGroup>
//     )
//   }
//
//   componentDidUpdate() {
//     if (this.state.previousPathname) {
//       this.setState({ previousPathname: null })
//     }
//   }
// }
//
// RouteCSSTransitionGroup.contextTypes = {
//   location: React.PropTypes.object
// }
