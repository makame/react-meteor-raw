AdminLayout = React.createClass({
  mixins: [ReactMeteorData, GetSubscribe],

  getMeteorData() {
    return this.getSubscribe('home_user', [{name: "self"}], function(data) {
      data.currentUser = Meteor.user()

      if (data.currentUser && data.currentUser.avatar) {
        data.currentUser.avatar_src = Avatars.findOne({_id:data.currentUser.avatar})
      }

      return data
    })
  },

  render() {
    var items = _.map(Admin.tables, function(item) {
      return (
        <Link className="item" key={item.name} to={ '/admin/table/' + item.name } activeClassName="active">
          {item.title}
        </Link>
      )
    })

    var itemsPop = (<PopupMenu>{items}</PopupMenu>)

    return (
      <div>
        <div className="top-menu container p90 block center">
          <div className="items-desktop">
            <Link className="header" to={"/admin"}>
              Admin
            </Link>
          </div>
          <PopupElement html={itemsPop} position="bottom center" offset={-0} className="item" on="click" back={true}>
            <span className="fa fa-bars"></span>
            Tables
          </PopupElement>
          <div className="space">
          </div>
        </div>

        <RouteChanger duration={150} currentUser={this.data.currentUser} tableName={this.props.params.tableName}>
          {this.props.children}
        </RouteChanger>
      </div>
    )
  }
})
