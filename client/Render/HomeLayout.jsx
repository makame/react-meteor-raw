HomeLayout = React.createClass({
  mixins: [ReactMeteorData, GetSubscribe],

  getMeteorData() {
    return this.getSubscribe('home_user', [{name: "self"}, {name: "notification"}], function(data) {
      data.currentUser = Meteor.user()

      data.notifications = Notification.find({}, {limit: 10, sort: {createdAt: -1}}).fetch()

      if (data.currentUser && data.currentUser.avatar) {
        data.currentUser.avatar_src = Avatars.findOne({_id:data.currentUser.avatar})
      }

      return data
    })
  },

  render() {
    var popup = (<UserPanelPopup currentUser={this.data.currentUser}/>)

    var items = [
      (
        <Link className="item" key="editor" to={"/editor"} activeClassName="active">
          Editor
        </Link>
      ),
      (
        <Link className="item" key="chat" to={"/chat"} activeClassName="active">
          Chat
        </Link>
      ),
      (
        <Link className="item" key="about" to={"/about"} activeClassName="active">
          About Me
        </Link>
      )
    ]

    var itemsPop = (<PopupMenu>{items}</PopupMenu>)

    return (
      <div>
        <div className="top-menu container p90 block center">
          <div className="items-desktop">
            <Link className="header" to={"/"}>
              RAW Kit
            </Link>
            {items}
          </div>
          <div className="items-mobile">
            <PopupElement html={itemsPop} position="bottom center" offset={-0} className="item" on="click" back={true}>
              <span className="fa fa-bars"></span>
              Menu
            </PopupElement>
          </div>
          <div className="space">
          </div>
          <PopupElement html={popup} position="bottom left" offset={-0} className="item" on="click" back={false}>
            <span className="fa fa-cog"></span>
            {this.data.currentUser ? this.data.currentUser.username : 'Login'}
          </PopupElement>
        </div>

        <RouteChanger duration={150} currentUser={this.data.currentUser} notifications={this.data.notifications}>
          {this.props.children}
        </RouteChanger>

        <NotificationsElem notifications={this.data.notifications}/>

        <div className="container align-center margin-top-2 margin-bottom-1">makame@mail.ru</div>
      </div>
    )
  }
})

UserPanelPopup = React.createClass({
  render() {
    var container = (<LoginPanel/>)

    if (this.props.currentUser) {
      container = (<UserPanel currentUser={this.props.currentUser}/>)
    }

    return (
      <div className="padding-top-2 padding-bottom-5 padding-horizontal-2">
        {container}
      </div>
    )
  }
})
