App = React.createClass({
  onClick() {
    windowService.hideFront()
  },

  handleResize: function(e) {
    windowService.hidePopup()
  },

  componentDidMount: function() {
    window.addEventListener('resize', this.handleResize);
  },

  componentWillUnmount: function() {
    window.removeEventListener('resize', this.handleResize);
  },

  render() {
    return (
      <div>
        <WindowContainer/>
        <LoadingContainer/>
        <div className="app" onClick={this.onClick}>
          {this.props.children}
        </div>
      </div>
    )
  }
});
