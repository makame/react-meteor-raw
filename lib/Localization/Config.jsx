i18n.setDefaultLanguage('en_US');

i18n.setLanguage('en');
moment.locale('en');

// if (Meteor.isClient) {
//   Session.setDefault("locale", 'en');
//
//   if (Cookie.get('locale'))
//   {
//     Session.set("locale", Cookie.get('locale'));
//     moment.locale(Cookie.get('locale'));
//     i18n.setLanguage(Cookie.get('locale'));
//   }
//   else {
//     Cookie.set('locale', 'en')
//     moment.locale(Cookie.get('locale'));
//     i18n.setLanguage(Cookie.get('locale'));
//   }
// }

if (Meteor.isServer) {
  var user = Meteor.users.findOne({_id: this.userId})
  if (user)
  {
    if (user.locale) {
      i18n.setLanguage(user.locale);
      moment.locale(user.locale);
    }
    else {
      i18n.setLanguage('en');
      moment.locale('en');
    }
  }
}
