i18n.map('ru', {
  map: {
    chats: 'Чаты',
    chat: 'Чат',
  },
  login: {
    login: 'Email / Логин',
    password: 'Пароль',
    remember: 'Запомнить',
    enter: 'Войти',
    message:
    {
      success: 'Вход выполнен успешно',
      error: 'Нет такой пары логин - пароль',
    }
  },
  registration: {
    username: 'Имя пользователя',
    email: 'Email',
    password: 'Пароль',
    password_one: 'Пароль еще раз',
    registration: 'Зарегистрироваться',
    message:
    {
      success: 'Регистрация прошла успешно! Вам выслано письмо для активации вашей учетной записи',
      error: 'Ошибка регистрации. Что то введено не верно',
      error_username_low: 'Имя пользователя должно быть больше {$1} символов',
      error_email_low: 'Email должен быть больше {$1} символов',
      error_login_exist: 'Данный логин занят',
      error_email_exist: 'Такой Email существует в системе',
      error_password_low: 'Пароль должен быть больше {$1} символов',
      error_password_one_wrong: 'Пароли не совпадают',
    }
  },
  recover_pass: {
    about: 'Для восстановления пароля требуется отправить письмо на ваш почтовый ящик',
    email: 'Email',
    send: 'Отправить'
  },
  dashboard: {
    was_here: 'был тут',
    send_activation_email: 'Отправить письмо для активации',
    logout: 'Выйти',
    message:
    {
      logout: 'Вы вышли из под своей учетной записи',
      activation_send: 'Письмо отправлено'
    }
  },
  notif: {
    delete: 'Убрать',
    clear: 'Очистить',
    no_notifs: 'У вас нет уведомлений'
  },
  notification: {
    error: 'Ошибка',
    warning: 'Предупреждение',
    success: 'Успешно',
    info: 'Уведомление'
  },
  settings: {
    change_password: {
      change_password: 'Изменить пароль',
      old_password: 'Старый пароль',
      new_password: 'Новый пароль',
      new_password_one: 'Новый пароль еще раз',
      change: 'Изменить',
      message:
      {
        error_password_low: 'Пароль должен быть больше 4 символов',
        error_password_one_wrong: 'Пароли не совпадают',
        error_password_wrong: 'Пароль введен не верно',
        success: 'Пароль был успешно изменен!'
      }
    }
  },
  verify_mail: {
    message:
    {
      wrong: 'Простите, ссылка устарела, попробуйте выслать письмо для активации повторно',
      error_email_was_activated: 'Почтовый ящик уже активирован',
      success: 'Спасибо! Ваш email был успешно активирован',
    },
    email: {
      subject: 'Подтвердите ваш Email',
      before_url: 'Для активации вашего аккаунта перейдите по следующей ссылке: <a href="',
      after_url: '">АКТИВИРОВАТЬ</a>'
    }
  },
  chat: {
    open: 'Открыть',
    no_chats: 'У вас нет переписок',
    create_chat: 'Создать чат',
    add_users: 'Добавить пользователей',
    chose_users: 'Выберете пользователя',
    write_message: 'Написать сообщение',
    send_message: 'Отправить',
    no_messages: 'Нет сообщений',
    unsubscribe: 'Отписаться',
    answer: 'Ответить',
    field: {
      header: {
        name: 'Название',
        placeholder: 'Введите название',
      },
      users: {
        name: 'Выберете участников',
        placeholder: 'Введите имя пользователя',
      },
      message: {
        name: 'Сообщение',
        placeholder: 'Введите сообщение',
      },
    },
    message: {
      success_create_chat: 'Чат успешно создан',
      error_empty_message: 'Введите сообщение',
      error_chat_not_exist: 'Нет такого чата',
      no_answer: "Ответ не найден"
    }
  },
  global: {
    load_more: 'Загрузить еще',
    submit: 'Подтвердить',
    cancel: 'Отмена',
    processing: 'Выполнение запроса...',
    search: 'Поиск',
    bad_connection: 'Соединение потеряно',
    multiselect: {
      addResult: 'Добавить <b>{term}</b>',
      count: '{count} выбрано',
      maxSelections: 'Максимум {maxCount}',
      noResults: 'Ничего не найдено.'
    },
    message: {
      error_not_login: 'Вы не зарегистрированы',
      lost_connection: 'Соединение прервано'
    }
  }
});

moment.locale('ru', {
  relativeTime : {
    future: "через %s",
    past:   "%s назад",
    s:  "секунду",
    ss:  "%s секунд",
    m:  "минуту",
    mm: "%d минут",
    h:  "час",
    hh: "%d часов",
    d:  "день",
    dd: "%d дней",
    M:  "месяц",
    MM: "%d месяцев",
    y:  "год",
    yy: "%d лет"
  }
});
