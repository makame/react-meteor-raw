i18n.map('en', {
  map: {
    chats: 'Chats',
    chat: 'Chat',
  },
  login: {
    login: 'Email / Login',
    password: 'Password',
    remember: 'Remember',
    enter: 'Enter',
    message:
    {
      success: 'Login success',
      error: 'We have not current login - password pair',
    }
  },
  registration: {
    username: 'Username',
    email: 'Email',
    password: 'Password',
    password_one: 'Repeat password',
    registration: 'Registration',
    message:
    {
      success: 'Registration successful! You will receive an email to activate your account.',
      error: 'Can\'t registration. Something wrong',
      error_username_low: 'Username should be more than {$1}',
      error_email_low: 'Email should be more than {$1}',
      error_last_name_low: 'Last name should be more than {$1}',
      error_first_name_low: 'First name should be more than {$1}',
      error_username_exist: 'This username is not available',
      error_email_exist: 'This email exists in the system',
      error_password_low: 'Password must be greater than {$1} characters',
      error_password_one_wrong: 'Password mismatch',
    }
  },
  recover_pass: {
    about: 'To recover your password is required to send a letter to your mailbox',
    email: 'Email',
    send: 'Send'
  },
  dashboard: {
    was_here: 'was here',
    send_activation_email: 'Send a letter to activate',
    logout: 'Exit',
    message:
    {
      logout: 'You went out of your account',
      activation_send: 'Letter sent'
    }
  },
  notif: {
    delete: 'Remove',
    clear: 'Clear',
    no_notifs: 'You have no notifications'
  },
  notification: {
    error: 'Error',
    warning: 'Warning',
    success: 'Success',
    info: 'Info'
  },
  settings: {
    change_password: {
      change_password: 'Change password',
      old_password: 'Old password',
      new_password: 'New password',
      new_password_one: 'Repeat new password',
      change: 'Change',
      message:
      {
        error_password_low: 'Password must be greater than {$1} characters',
        error_password_one_wrong: 'Password mismatch',
        error_password_wrong: 'The password is not right',
        success: 'The password was successfully changed!'
      }
    }
  },
  verify_mail: {
    message:
    {
      wrong: 'Sorry, the link is out of date, try to send a letter to activate again',
      error_email_was_activated: 'The mailbox is already activated',
      success: 'Thank you! Your email has been successfully activated',
    },
    email: {
      subject: 'Confirm your Email',
      before_url: 'To activate your account click on the following link: <a href="',
      after_url: '">ACTIVATE</a>'
    }
  },
  chat: {
    open: 'Open',
    no_chats: 'You have no correspondences',
    create_chat: 'Create a chat',
    add_users: 'Add users',
    chose_users: 'Choose a user',
    write_message: 'To write a message',
    send_message: 'Send',
    no_messages: 'No messages',
    unsubscribe: 'Unsubscribe',
    answer: 'Answer',
    field: {
      header: {
        name: 'Name',
        placeholder: 'Enter name',
      },
      users: {
        name: 'More members',
        placeholder: 'Enter user name',
      },
      message: {
        name: 'Message',
        placeholder: 'Enter your message',
      },
    },
    message: {
      success_create_chat: 'Successfully created chat',
      error_empty_message: 'Enter your message',
      error_chat_not_exist: 'Chat is not exist',
      no_answer: "Answer did't find"
    }
  },
  global: {
    load_more: 'Load more',
    submit: 'Submit',
    cancel: 'Cancel',
    processing: 'Running a query...',
    search: 'Search',
    bad_connection: 'Connection lost',
    multiselect: {
      addResult: 'Add <b>{term}</b>',
      count: '{count} selected',
      maxSelections: 'Maximun {maxCount}',
      noResults: 'Nothing Found.'
    },
    message: {
      error_not_login: 'You are not registred',
      lost_connection: 'Connection lost'
    }
  }
});
