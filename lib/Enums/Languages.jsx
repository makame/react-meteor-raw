Languages = [
  'ru',
  'en',
  'es',
  'gr',
  'fr',
  'ch',
]

Language_name = function(value) {
  if (value == 'ru') {
    return 'Russian';
  }
  else if (value == 'en') {
    return 'English';
  }
  else if (value == 'es') {
    return 'Spanish';
  }
  else if (value == 'gr') {
    return 'German';
  }
  else if (value == 'fr') {
    return 'French';
  }
  else if (value == 'ch') {
    return 'Chinese';
  }
}
