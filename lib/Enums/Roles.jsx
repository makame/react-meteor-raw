Roles = [
  'user',
  'moder',
  'admin',
]

Role_name = function(value) {
  if (value == 'user') {
    return 'User';
  }
  else if (value == 'moder') {
    return 'Moderator';
  }
  else if (value == 'admin') {
    return 'Admin';
  }
}
