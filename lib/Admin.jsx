Admin = new class Admin {
  constructor() {
    this.tables = {}
  }

  registryTable(name, title, collection) {
    this.tables[name] = {
      name: name,
      title: title,
      collection: collection,
    }
  }

  getCollection(name) {
    return this.tables[name].collection
  }

  getTitle(name) {
    return this.tables[name].title
  }
}()

Admin.registryTable('users', 'Users', Meteor.users)

if (Meteor.isServer) {
  Meteor.publishComposite("admin_table", function(data){
    return {
      find: function() {
        var user_id = Meteor.users.findOne({_id: this.userId, roles: {$all: ["admin"]}})
        if (user_id) {
          return Admin.getCollection(data.name).find(data.find, {limit: data.limit, sort: data.sort})
        }
        else {
          return undefined
        }
      },
    }
  })

  Meteor.publishComposite("admin_table_id", function(data){
    return {
      find: function() {
        var user_id = Meteor.users.findOne({_id: this.userId, roles: {$all: ["admin"]}})
        if (user_id) {
          return Admin.getCollection(data.name).find({_id: data._id})
        }
        else {
          return undefined
        }
      },
    }
  })

  Meteor.methods({
    admin_update: function (data) {
      var user_id = Meteor.users.findOne({_id: Meteor.userId(), roles: {$all: ["admin"]}})
      if (user_id) {
        Admin.getCollection(data.name).update({_id: data._id}, {$set: data.item})
      }
    },

    admin_remove: function (data) {
      var user_id = Meteor.users.findOne({_id: Meteor.userId(), roles: {$all: ["admin"]}})
      if (user_id) {
        Admin.getCollection(data.name).remove({_id: {$in : data._ids}})
      }
    },
  })

}
