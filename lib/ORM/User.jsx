Schema.UserEmail = new SimpleSchema({
  address: {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
  },
  verified: {
    type: Boolean
  },
})

Schema.User = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    index: false,
    unique: false,
  },
  username: {
    type: String,
    label: 'Username',
    custom: function () {
      check(this.value, String)
      var regexp = /^[a-z0-9A-Z_]{3,15}$/
      if (regexp.test(this.value))
      {
        return true
      }
      else {
        throw new Meteor.Error(403, "Username do not match to [a-z, 0-9, A-Z_] from {3,15}")
      }
    }
  },
  emails: {
    type: [Schema.UserEmail],
    optional: true
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date()
      } else if (this.isUpsert) {
        return {$setOnInsert: new Date()}
      } else {
        this.unset()
      }
    }
  },
  profile: {
    type: Object,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return {}
      }
    },
  },
  services: {
    type: Object,
    optional: true,
    blackbox: true
  },
  roles: {
    type: [String],
    allowedValues: Roles,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        if (!Meteor.users.findOne({})) {
          return ['user', 'admin']
        }
        return ['user']
      }
    },
  },
  first_name: {
    type: String,
    regEx: /^[a-zA-Z-]{0,25}$/,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return ''
      }
    },
  },
  last_name: {
    type: String,
    regEx: /^[a-zA-Z]{0,25}$/,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return ''
      }
    },
  },
  avatar: {
    type: String,
    optional: true,
  },
  avatar_raw: {
    type: String,
    optional: true,
  },
  language: {
    type: [String],
    allowedValues: Languages,
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return []
      }
    },
  },
  birthday: {
    type: Date,
    optional: true
  },
  gender: {
    type: String,
    allowedValues: ['male', 'female'],
    optional: true
  },
  bio: {
    type: String,
    optional: true,
    max: 500,
    min: 0,
  },
  status: {
    type: Object,
    optional: true,
    blackbox: true
  },
  locale: {
    type: String,
    allowedValues: ['en', 'ru'],
    optional: true,
    autoValue: function () {
      if (this.isInsert) {
        return 'en'
      }
    },
  }
})

Schema.User.messages({
  "username": "Username do not match to [a-z, 0-9, A-Z_] from {3,15}"
})

Meteor.users.attachSchema(Schema.User)

PasswordReqExp = Match.Where(function(str){
  check(str, String)
  var regexp = /^[a-zA-Z]\w{3,14}$/
  if (regexp.test(str))
  {
    return true
  }
  else {
    throw new Meteor.Error(403, "The password's first character must be a letter, it must contain at least 4 characters and no more than 15 characters and no characters other than letters, numbers and the underscore may be used")
  }
})
