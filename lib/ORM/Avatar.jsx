var avatar_store = new FS.Store.GridFS("images", {
  filter: {
    maxSize: 10485760,
    allow: {
      contentTypes: ['image/*'],
      extensions: ['png', 'jpg'],
    },
  },
});

Avatars = new FS.Collection("avatars", {
  stores: [avatar_store],
});
