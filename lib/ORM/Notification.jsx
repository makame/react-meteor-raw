Schema.Notification = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    index: false,
    unique: false,
  },
  userId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  title: {
    type: String,
    custom: function () {
      check(this.value, String)
      var regexp = /^[a-z0-9A-Z_]{3,15}$/
      if (regexp.test(this.value))
      {
        return true
      }
      else {
        throw new Meteor.Error(403, "Title do not match to [a-z, 0-9, A-Z_] from {3,15}")
      }
    }
  },
  seen: {
    type: Boolean,
    autoValue: function() {
      if (this.isInsert) {
        return false
      }
    }
  },
  message: {
    type: String,
    custom: function () {
      check(this.value, String)
      var regexp = /^[a-z0-9A-Z_]{3,30}$/
      if (regexp.test(this.value))
      {
        return true
      }
      else {
        throw new Meteor.Error(403, "Message do not match to [a-z, 0-9, A-Z_] from {3,30}")
      }
    }
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date()
      } else if (this.isUpsert) {
        return {
          $setOnInsert: new Date()
        }
      } else {
        this.unset()
      }
    }
  },
})

Notification = new Mongo.Collection("notification")

Notification.allow({
  insert: function(){
    return true
  },
  update: function(){
    return true
  },
  remove: function(){
    return true
  }
})
